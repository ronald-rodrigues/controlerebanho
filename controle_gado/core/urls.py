from django.urls import path
from core.views import *

app_name = 'core'

urlpatterns = [
    path('', index, name='index'),
    path('login', login_empresa, name='login'),
    path('logout', logout_empresa, name='logout'),
    path('cadastro', cadastro_empresa, name='cadastro'),
    path('anonimo', consulta_anonima, name='anonimo'),
    path('acessocnpj', consulta_cnpj, name='acessocnpj'),
    path('gadocontrole', gado_controle, name='gadocontrole'),
    path('gadoupdate/<int:id>', gado_update, name='gadoupdate'),
    path('gadodelete/<int:id>', gado_delete, name='gadodelete'),
    path('gadosave', gado_save, name='gadosave'),

]