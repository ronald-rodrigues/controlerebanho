# -*- coding: utf-8 -*-
from core.models import User_fazenda, Gado
from django.forms import ModelForm
from django.forms.widgets import *

import jwt


class FormFazenda(ModelForm):
    class Meta:
        model = User_fazenda
        fields = ['nome_empresa', 'username', 'password', 'token', 'cnpj']
        widgets = {
            'nome_empresa': TextInput(attrs={'placeholder': 'Employee', 'class': 'form-control'}),
            'username': TextInput(attrs={'placeholder': 'Username', 'class': 'form-control'}),
            'password': PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control'}),
            'token': TextInput(attrs={'placeholder': 'Token', 'class': 'form-control', 'value': 'none'}),
            'cnpj': NumberInput(attrs={'placeholder': 'Type your cnpj', 'class': 'form-control'})
        }

    def save(self, commit=True):
        fazenda = super(FormFazenda, self).save(commit=False)
        fazenda.set_password(self.cleaned_data['password'])
        fazenda.username = self.cleaned_data['username']

        payload = {
            'username': fazenda.username,
        }
        fazenda.token = jwt.encode(payload, 'jetbov', algorithm='HS256')
        if commit:
            fazenda.save()
        return fazenda


class GadoForm(ModelForm):
    class Meta:
        model = Gado
        fields = '__all__'
        widgets = {
            'peso': NumberInput(attrs={'class': 'form-control'}),
            'brinco_id': NumberInput(attrs={'class': 'form-control'}),
        }
