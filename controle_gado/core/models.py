from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User
# modelo de entidade para a fazenda, algo simples que represente o acesso
# e o relacionamento com os especimes seu rebanho


class Gado(models.Model):
    peso = models.FloatField()
    data_cad = models.DateTimeField(auto_now_add=True)
    brinco_id = models.CharField(max_length=100)
    fazenda = models.ForeignKey('User_fazenda', on_delete=models.CASCADE, default=None, null=True)


class User_fazenda(User):
    nome_empresa = models.CharField(max_length=100)
    token = models.CharField(max_length=500)
    cnpj = models.IntegerField(null=True)


