from django.shortcuts import render, HttpResponse, redirect
from core.forms import FormFazenda, GadoForm
from django.contrib.auth import authenticate, login, logout, get_user
from django.contrib.auth.decorators import login_required
from core.models import *

import jwt


def index(request):
    formulario = FormFazenda
    return render(request, 'core/index.html', {'form': formulario})


def login_empresa(request):
    logout(request)
    form = FormFazenda(request.POST or None)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        fazenda = authenticate(request, username=username, password=password)
        if fazenda is not None:
            login(request, fazenda)
            return redirect('/gadocontrole')
        else:
            return HttpResponse('Usuario nao cadastrado')
    return render(request, 'core/login.html', {'form': form})


def logout_empresa(request):
    logout(request)
    return redirect('/')


def cadastro_empresa(request):
    form = FormFazenda(request.POST or None)
    if request.method == 'POST':
        form.save()
        return redirect('/login')
    return render(request, 'core/cadastro.html', {'form': form})


def consulta_cnpj(request):
    if request.method == 'POST':
        fazenda = User_fazenda.objects.get(cnpj=request.POST.get('cnpj'))
        rebanho = Gado.objects.filter(fazenda_id=fazenda.id)
        return render(request, 'core/listagem_animais.html', {'rebanho': rebanho})
    return render(request, 'core/acesso_cnpj.html')


def consulta_anonima(request):
    if request.method == 'POST':
        user = jwt.decode(request.POST.get('token'), 'jetbov', algorithms=['HS256'])
        fazenda = User_fazenda.objects.get(username=user['username'])
        rebanho = Gado.objects.filter(fazenda_id=fazenda.id, brinco_id=request.POST.get('brinco_id'))
        return render(request, 'core/listagem_animais.html', {'rebanho': rebanho})

    return render(request, 'core/acesso_anonimo.html')


@login_required
def gado_controle(request):
    user = get_user(request)
    rebanho = Gado.objects.filter(fazenda_id=user.id)
    templatevars = {
        'form': GadoForm(),
        'rebanho': rebanho,
        'fazenda': User_fazenda.objects.get(username=user.username),
    }
    return render(request, 'core/gado_controle.html', templatevars)


@login_required
def gado_save(request):
    if request.method == 'POST':
        gado = Gado()
        gado.peso = request.POST.get('peso')
        gado.brinco_id = request.POST.get('brinco_id')
        gado.fazenda = User_fazenda.objects.get(username=get_user(request).username)
        gado.save()
        return redirect('/gadocontrole')
    return redirect('/gadocontrole')


@login_required
def gado_update(request, id):
    gadoitem = Gado.objects.get(id=id)
    gadoitem.peso = request.POST.get('peso')
    gadoitem.brinco_id = request.POST.get('brinco_id')
    gadoitem.save()
    return redirect('/gadocontrole')


@login_required
def gado_delete(request, id):
    gadoitem = Gado.objects.get(id=id)
    gadoitem.delete()
    return redirect('/gadocontrole')
