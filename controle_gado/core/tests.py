from django.test import TestCase
from django.urls import reverse


class TestClientView(TestCase):
    fixtures = ['users.json']

    def setUp(self):
        self.response = self.client.login(username='ronald', password='123')

    def test_profile_success(self):
        self.response = self.client.get(reverse('core:gadocontrole'))
        self.assertEqual(200, self.response.status_code)
        self.assertTemplateUsed(self.response, 'core/gado_controle.html')
